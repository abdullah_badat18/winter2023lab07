import java.util.Scanner;
public class TicTacToeGame{
	public static void main(String[] args)
	{
		System.out.println("Welcome to the Tic-Tac-Toe game!");
		System.out.println();
		
		Scanner sc = new Scanner(System.in);
		Board myBoard = new Board();
		
		boolean gameOver = false;
		int player = 1;
		
		Square playerToken = Square.X;
		
		while(!gameOver)
		{
			System.out.println(myBoard);
			
			if(player == 1)
			{
				playerToken = Square.X;
			}
			else
			{
				playerToken = Square.O;
			}	
			
			boolean test = true;
			int row = 0;
			int col = 0;
			
			while(test)
			{
				System.out.println("Enter the row number: ");
				row = sc.nextInt();
		
				System.out.println("Enter the column number: ");
				col = sc.nextInt();
			
				if(!(myBoard.placeToken(row, col, playerToken)))
				{
					System.out.println("Invalid number! Please re-enter the value: ");
				}
				else
				{
					test = false;
				}
			}	
			myBoard.placeToken(row, col, playerToken);
			
			if(myBoard.checkIfWinning(playerToken))
			{
				System.out.println("Player #"+player+" is the winner");
				gameOver = true;				
			}
			else if(myBoard.checkIfFull())
			{
				System.out.println("It’s a tie!");
				gameOver = true;
			}
			else{
				player += 1;
				
				if(player > 2)
				{
					player = 1;
				}
			}
		}	
	}
}


