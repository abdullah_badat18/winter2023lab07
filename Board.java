public class Board{
	
	private Square[][] tictactoeBoard;
	
	public Board(){
		
		this.tictactoeBoard = new Square[3][3];
		initializerHelper();
	}
	
	public String toString(){
		
		String position = "  ";
		for(int i=0; i<this.tictactoeBoard.length; i++)
		{
			position+= i+" ";
		}
		
		for(int i=0; i<this.tictactoeBoard.length; i++)
		{
			position += "\n"+i +" ";
			for(int j=0; j<this.tictactoeBoard[i].length; j++)
			{
				position += this.tictactoeBoard[i][j] + " ";
			}
		}
		return position; 
	}
	
	private void initializerHelper()
	{	
		for(int i=0; i<this.tictactoeBoard.length; i++)
		{
			for(int j=0; j<this.tictactoeBoard[i].length; j++)
			{
				this.tictactoeBoard[i][j] = Square.BLANK; 
			}
		}
	}
	
	public boolean placeToken(int row, int col, Square playerToken)
	{
		boolean result = false;
		if (row < 0 || row > this.tictactoeBoard.length - 1)
		{
			return false;
		}
		
		if (col < 0 || col > this.tictactoeBoard.length - 1)
		{
			return false;
		}
		
		if(this.tictactoeBoard[row][col] == Square.BLANK)
		{
			this.tictactoeBoard[row][col] = playerToken;
			result = true;
		}
		return result;
	}
	
	public boolean checkIfFull()
	{
		boolean result = true;
		for(int i=0; i<this.tictactoeBoard.length; i++)
		{
			for(int j=0; j<this.tictactoeBoard[i].length; j++)
			{
				if(this.tictactoeBoard[i][j] == Square.BLANK)
				{
					result = false;
				}
			}
		}
		return result;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken)
	{
		boolean result = false;
		for(int i=0; i<this.tictactoeBoard.length; i++)
		{
		    if(this.tictactoeBoard[i][0] == playerToken && this.tictactoeBoard[i][1] == playerToken && this.tictactoeBoard[i][2] == playerToken)
			{
					return true;
			}
		}
		return result;
	}
	
	private boolean checkIfWinningVertical(Square playerToken)
	{
		boolean result = false;
		for(int i=0; i<this.tictactoeBoard.length; i++)
		{
				if(this.tictactoeBoard[0][i] == playerToken && this.tictactoeBoard[1][i] == playerToken && this.tictactoeBoard[2][i] == playerToken)
				{
					result = true;
				}
		}
		return result;
	}
	
	public boolean checkIfWinning(Square playerToken)
	{
		boolean result = false;
		if(checkIfWinningVertical(playerToken) || checkIfWinningHorizontal(playerToken))
		{
			return true;
		}
		return result;
	}
}